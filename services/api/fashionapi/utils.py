from typing import Tuple, Optional
import logging
import requests
import numpy as np
import tensorflow as tf

logger = logging.getLogger(__name__)
MODEL_THRESHOLD = 0.3


def read_image_from_url(
    url: str
) -> tf.Tensor:
    """Read image from given url.

    Parameters
    ----------
    url : str
        Url of image to download and process

    Returns
    -------
    tensorflow.Tensor
        Image tensor
    """
    return tf.image.decode_jpeg(
        requests.get(url).content, channels=3, name="jpeg_reader"
    )


def preprocess_image(
    image: tf.Tensor, size: Tuple[int, int] = (28, 28)
):
    """Preprocess image.

    Parameters
    ----------
    url : str
        Url of image to download and process
    size : tuple, optional
        Output size of image, by default (28, 28)

    Returns
    -------
    tensorflow.Tensor
        Image tensor
    """
    float_caster = tf.cast(image, tf.float32)
    resized = tf.image.resize(
        float_caster,
        size=size,
        method=tf.image.ResizeMethod.BILINEAR,
        preserve_aspect_ratio=False, antialias=True)
    gray_image = tf.image.rgb_to_grayscale(
        resized, name=None
    )
    gray_image = tf.expand_dims(gray_image, 0)
    return tf.cast(gray_image, tf.float32) / - 255. + 1


def predict_class(model: tf.keras.Model, image: tf.Tensor) -> Optional[int]:
    """Predict image class using provided model.

    Parameters
    ----------
    model : tf.keras.Model
        Model for prediction
    image : tf.Tensor
        Image for prediction

    Returns
    -------
    Optional[int]
        Result of prediction, or null if probability was below THRESHOLD
    """
    probs = model.predict(image)
    logger.debug(f'Probabilities: {probs}')
    max_prob = np.max(probs)
    logger.debug(f'Max probability: {max_prob}')
    return int(np.argmax(probs)) if max_prob > MODEL_THRESHOLD else None
