from flask import abort, Flask, request
from flask_restful import Api, Resource
import logging
import tensorflow as tf
from tensorflow.errors import InvalidArgumentError
from fashionapi.utils import (
    read_image_from_url,
    predict_class,
    preprocess_image
)
from marshmallow import Schema, fields


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)


class UrlQuerySchema(Schema):
    url = fields.Url(required=True)


app = Flask(__name__)
api = Api(app, catch_all_404s=True)
url_schema = UrlQuerySchema()
MODEL = tf.keras.models.load_model('fashionapi/resources/fashion')


class ConvolutionalModel(Resource):
    def get(self):
        url = request.args.get('url')
        logging.debug(f'url: {url}')
        errors = url_schema.validate(request.args)
        if errors:
            abort(400, str(errors))
        try:
            image = read_image_from_url(url)
        except InvalidArgumentError:
            abort(400, "Provided url is incorrect")

        image = preprocess_image(image)
        prediction = predict_class(MODEL, image)
        logging.debug(f'Prediction value: {prediction}')
        if prediction is not None:
            return {"class": prediction}, 200
        else:
            abort(500, "No clothing detected.")


api.add_resource(ConvolutionalModel, '/v1/')

if __name__ == '__main__':
    app.run(debug=True)
