from fashionapi.utils import predict_class, preprocess_image
import numpy as np
import pytest
import tensorflow as tf


@pytest.mark.parametrize(
    'input_, size, output',
    [
        (
            tf.convert_to_tensor(np.ones((5, 5, 3)) * 255),
            (1, 1),
            np.array([[[[0]]]])
        ),
        (
            tf.convert_to_tensor(np.zeros((5, 5, 3))),
            (1, 1),
            np.ones([1, 1, 1, 1])
        ),
        (
            tf.convert_to_tensor(np.zeros((5, 5, 3))),
            (5, 5),
            np.ones((1, 5, 5, 1))
        ),
        (
            tf.convert_to_tensor(np.zeros((5, 5, 3))),
            (3, 3),
            np.ones((1, 3, 3, 1))
        )
    ]
)
def test_preprocess_image(
    input_, size, output
):
    image = preprocess_image(input_, size)
    np.testing.assert_almost_equal(image.numpy(), output, decimal=3)


@pytest.mark.parametrize(
    'prediction, expected',
    [
        (
            np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
            0
        ),
        (
            np.array([0, 0, 0, 0.4, 0.2, 0.2, 0.2, 0, 0, 0, 0]),
            3
        ),
        (
            np.array([0, 0, 0.09, 0.2, 0.2, 0.2, 0.31, 0, 0, 0, 0]),
            6
        ),
        (
            np.array([0, 0, 0.4, 0.4, 0.1, 0.1, 0, 0, 0, 0, 0]),
            2
        )
    ]
)
def test_predict_class_success(prediction, expected):
    class Model:
        def predict(self, image):
            return prediction

    model = Model()
    result = predict_class(model, None)
    assert result == expected


@pytest.mark.parametrize(
    'prediction',
    [
        np.ones(10) * 0.1,
        np.array([0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0, 0, 0]),
        np.array([0, 0.2, 0, 0.3, 0.2, 0, 0.2, 0, 0, 0])
    ]
)
def test_predict_class_failure(prediction):
    class Model:
        def predict(self, image):
            return prediction

    model = Model()
    result = predict_class(model, None)
    assert result is None
