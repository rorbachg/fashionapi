# Fashion REST API

## Author
Grzegorz Rorbach (rorbach.g@gmail.com)

## About
This is a project of REST API classifying photos of clothings into MNIST-fashion classes. REST API is written in Flask RESTful running with Gunicorn WSGI and dockerized. Live version is deployed to AWS using Elastic Beanstalk with nginx as reverse proxy. Live version is available under http://fashion-rest-api.eu-west-1.elasticbeanstalk.com/v1/

## Running locally
API can be run locally in two ways:

1. Using Flask developement server:
```{bash}
$ cd services/api
$ poetry run python -m fashionapi.app
```
2. Using Gunicorn server with Docker (Please ensure that docker is installed on your system and is accessable from command line)

    * On Linux/Mac:
    ```{bash}
    (without cd)
    $ make run_server
    ```
    * On Windows:
    ```
    (without cd)
    run_server.bat
    ```

## Interacting with API
API exposes single endpoint at `/v1/`. In order to make a prediction, a client should specify GET request with `url` parameter:
```
{hostname}/v1/?url="example_url.jpg"
```
An API should response either with a JSON response containing `class` section:
```
{"class": 7}
```

or error message (with adequate error code)
```
{"message": "Provided url is incorrect"}
```
